﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using vega.Controllers.Resources;
using vega.Core.Models;
using vega.Persistance;

namespace vega.Controllers
{
    [Produces("application/json")]
    public class FeaturesController : Controller
    {
        private readonly VegaDbContext _context;
        private readonly IMapper _mapper;

        public FeaturesController(VegaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet("api/features")]
        public IEnumerable<KeyValuePairResource> GetFeatures()
        {
            var features = _context.Features.ToList();
            return _mapper.Map<List<Feature>, List<KeyValuePairResource>>(features);
        }
    }
}